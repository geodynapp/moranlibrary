//
//  moran.h
//  libreria MoranOpenMP
//
//  Created by Álvaro Lozano Rojo on 31/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#ifndef libreria_MoranOpenMP_moran_h
#define libreria_MoranOpenMP_moran_h

#include <stdint.h>

#define __MSG_CORTO__ "[Moran OPM] r = %.100g -> rho_A = %.100g\n"


typedef unsigned char uchar;
typedef unsigned long ulong;
typedef uint64_t FixP32_32;

#define pow2_16 65536
#define pow2_32 4294967296ull
#define shift1_32 4294967296ull


//Conjuntos
int busca  ( int   *C, int N, int pos);
int ubusca ( uchar *C, int N, int pos);
int rebusca( int   *C, int N, int pos);



#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
extern "C" {
#endif

    int EvolutivaGrafosAceleradoRTV (
                                     double *probabilidad_fijacion, //Esto es el valor de retorno - asumo que hay memoria reservada
                                     double *tiempoT,               //Tiempo medio en acabar (retorno)
                                     double *err_tiempoT,           //El varianza/tamaño de lo anterior (retorno)
                                     double *tiempoE,               //Tiempo medio en acabar con exito (retorno)
                                     double *err_tiempoE,           //El varianza/tama de lo anterior (retorno)
                                     double *tiempoF,               //Tiempo medio en acabar sin exito (retorno)
                                     double *err_tiempoF,           //El varianza/tama de lo anterior (retorno)
                                     const int *G_Vecinos,          //Matriz de vecinos N x N_1
                                     const int *G_NVecinos,         //Vector de cantidad de vecinos N
                                     const int N,                   //Orden del grafo
                                     const int N_1,                 //Valencia máxima
                                     const int G_Intentos,          //Numero de intentos para calcular la probabilidad de fijación
                                     const double rMin,             //Valor minimo de r
                                     const double rMax,             //Valor máximo de r (no se llega)
                                     const int Num_r                //Número de pruebas entre rMin y rMax
                                     );

    
    int EvolutivaGrafosAcelerado (
                         double *probabilidad_fijacion, //Esto es el valor de retorno
                         const int *G_Vecinos,          //Matriz de vecinos N x N_1
                         const int *G_NVecinos,         //Vector de cantidad de vecinos N
                         const int N,                   //Orden del grafo
                         const int N_1,                 //Valencia máxima
                         const int G_Intentos,          //Numero de intentos para calcular la probabilidad de fijación
                         const double rMin,             //Valor minimo de r
                         const double rMax,             //Valor máximo de r (no se llega)
                         const int Num_r                //Número de pruebas entre rMin y rMax
                         );


    int EvolutivaGrafos (
                         double *probabilidad_fijacion, //Esto es el valor de retorno
                         const int *G_Vecinos,          //Matriz de vecinos N x N_1
                         const int *G_NVecinos,         //Vector de cantidad de vecinos N
                         const int N,                   //Orden del grafo
                         const int N_1,                 //Valencia máxima
                         const int G_Intentos,          //Numero de intentos para calcular la probabilidad de fijación
                         const double rMin,             //Valor minimo de r
                         const double rMax,             //Valor máximo de r (no se llega)
                         const int Num_r                //Número de pruebas entre rMin y rMax
                        );

    int EvolutivaGrafosTiempos (
                                double *probabilidad_fijacion, //Esto es el valor de retorno
                                int* tiempo,
                                const int *G_Vecinos,          //Matriz de vecinos N x N_1
                                const int *G_NVecinos,         //Vector de cantidad de vecinos N
                                const int N,                   //Orden del grafo
                                const int N_1,                 //Valencia máxima
                                const int G_Intentos,          //Numero de intentos para calcular la probabilidad de fijación
                                const double rMin,             //Valor minimo de r
                                const double rMax,             //Valor máximo de r (no se llega)
                                const int Num_r                //Número de pruebas entre rMin y rMax
                               );

    int EvolutivaGrafosRTV (
                            double *probabilidad_fijacion, //Probabilidad de fijacion (retorno)
                            double *tiempoT,               //Tiempo medio en acabar (retorno)
                            double *err_tiempoT,           //El varianza/tamaño de lo anterior (retorno)
                            double *tiempoE,               //Tiempo medio en acabar con exito (retorno)
                            double *err_tiempoE,           //El varianza/tama de lo anterior (retorno)
                            double *tiempoF,               //Tiempo medio en acabar sin exito (retorno)
                            double *err_tiempoF,           //El varianza/tama de lo anterior (retorno)
                            double *R,                     //Los valores de r
                            const int Num_r,
                            const int *G_Vecinos,          //Matriz de vecinos N x N_1
                            const int *G_NVecinos,         //Vector de cantidad de vecinos N
                            const int N,                   //Orden del grafo
                            const int N_1,                 //Valencia máxima
                            const int G_Intentos           //Numero de intentos para calcular
                            //la probabilidad de fijación
                           );


    int SizeInt();



#ifdef __cplusplus /* If this is a C++ compiler, use C linkage */
}
#endif



#endif
