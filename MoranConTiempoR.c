#include <stdlib.h>
#include <stdio.h>

#include "well1024.h"
#include "moran.h"

    int EvolutivaGrafosRTV (
                             double *probabilidad_fijacion, //Probabilidad de fijacion (retorno) 
                             double *tiempoT,               //Tiempo medio en acabar (retorno)
                             double *err_tiempoT,           //El varianza/tamaño de lo anterior (retorno)
                             double *tiempoE,               //Tiempo medio en acabar con exito (retorno)
                             double *err_tiempoE,           //El varianza/tama de lo anterior (retorno)
                             double *tiempoF,               //Tiempo medio en acabar sin exito (retorno)
                             double *err_tiempoF,           //El varianza/tama de lo anterior (retorno)
                             double *R,                     //Los valores de r
                             const int Num_r, 
                             const int *G_Vecinos,          //Matriz de vecinos N x N_1
                             const int *G_NVecinos,         //Vector de cantidad de vecinos N
                             const int N,                   //Orden del grafo
                             const int N_1,                 //Valencia máxima
                             const int G_Intentos           //Numero de intentos para calcular
                                                            //la probabilidad de fijación
                            ) {
    
    int j;
    
   
    //Inicializamos unas semillas para los generadores aleatorios
    #pragma omp parallel for private(j) shared(probabilidad_fijacion,R,tiempoT,err_tiempoT,tiempoE,err_tiempoE,tiempoF,err_tiempoF)
	for( j=0 ; j < Num_r ; j++ ) {
        double r = R[j];
        
        well1024 rng;
		well1024_init_devrandom(&rng);
        
		int exitos = 0;
        
        int *etiquetas = (int*)malloc(sizeof(int) * N);  // P_ETIQUETAS
        int *CMutantes = (int*)malloc(sizeof(int) * N);  // P_RANGO0;
        int *CNormales = (int*)malloc(sizeof(int) * N);  // P_RANGO1;
        
        double i_tiempoT = 0;
        double i_var_tiempoT = 0;
        double i_tiempoE = 0;
        double i_var_tiempoE = 0;
        double i_tiempoF = 0;
        double i_var_tiempoF = 0;
        double delta;
        
        int Intentos = G_Intentos;
        
		for( int i=0 ; i<Intentos ; i++ ) {
            
			//El grafo
            for( int x = 0 ; x < N ; x++){
                etiquetas[x] = 0;
                CNormales[x] = 1;
                CMutantes[x] = 0;
            }
            
			//Ponemos el primer mutante
			int kk;
			int k = well1024_next_int(&rng, N);
			int NMutantes = 1;
			etiquetas[k] = TRUE;
			CNormales[k] = FALSE;
			CMutantes[k] = TRUE;
            
            int iteraciones = 0;
            
			while( (NMutantes < N) && (NMutantes > 0) ) {
                iteraciones++;
                
				/*Tomamos uno al azar para reproducirse
                 Usamos los conjuntos CNormales y CMutantes para buscar los elementos al azar.
                 P[CNormales] =  #CNormales/(#CNormales + r#CMutantes)
                 P[CMutantes] = r#CMutantes/(#CNormales + r#CMutantes)              */
				if( well1024_next_double(&rng) < ((double)(N-NMutantes))/(N-NMutantes + r*NMutantes) ){
					k = busca(CNormales, N, well1024_next_int(&rng, N-NMutantes));
				} else {
					k = busca(CMutantes, N, well1024_next_int(&rng, NMutantes));
				}
                
				//Tomamos otro al azar para morir
				kk = G_Vecinos[ well1024_next_int(&rng, G_NVecinos[k]) + k * N_1 ];
				
				if( etiquetas[k] && !etiquetas[kk] ) {
					NMutantes++;
					CMutantes[kk] = TRUE;
					CNormales[kk] = FALSE;
					etiquetas[kk] = etiquetas[k];
				} else if( !etiquetas[k] && etiquetas[kk] ) {
					NMutantes--;
					CMutantes[kk] = FALSE;
					CNormales[kk] = TRUE;
					etiquetas[kk] = etiquetas[k];
				}
				//En el resto de casos no hay nada que hacer
			} 
			
            //Estadisticas de tiempo generales
            //Usamos el algoritmo para la varianza de Knuth
            delta = (double)iteraciones - i_tiempoT;
            i_tiempoT = i_tiempoT + delta/(i + 1);
            i_var_tiempoT = i_var_tiempoT + delta*(iteraciones - i_tiempoT);
            
			if(NMutantes) {
				exitos++;
                delta = (double)iteraciones - i_tiempoE;
                i_tiempoE = i_tiempoE + delta/exitos;
                i_var_tiempoE = i_var_tiempoE + delta*(iteraciones - i_tiempoE);
            } else {
                delta = (double)iteraciones - i_tiempoF;
                i_tiempoF = i_tiempoF + delta/(i - exitos + 1);
                i_var_tiempoF = i_var_tiempoF + delta*(iteraciones - i_tiempoF);
            }
		}
        //Limpiamos memoria
        free(etiquetas);
        free(CMutantes);
        free(CNormales);
        
        //Guardamos los resultados
		probabilidad_fijacion[j] = ((double)exitos)/Intentos;
        tiempoT[j] = i_tiempoT;
        tiempoE[j] = i_tiempoE;
        tiempoF[j] = i_tiempoF;
        
        err_tiempoT[j] = i_var_tiempoT/((double)Intentos*Intentos);
        err_tiempoE[j] = (exitos)?(i_var_tiempoE/((double)exitos*exitos)):0;
        err_tiempoF[j] = (Intentos-exitos)?(i_var_tiempoF/(((double)(Intentos-exitos))*(Intentos-exitos))):0;
	}
	
    //y nos vamos
    return 1;
}



