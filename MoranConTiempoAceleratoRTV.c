//
//  MoranConTiempoAceleratoRTV.c
//  MoranOpenMP
//
//  Created by Álvaro Lozano Rojo on 11/03/13.
//
//

#include <stdlib.h>
//#include <time.h>
#include <stdio.h>
#include "well1024.h"
#include "moran.h"

int EvolutivaGrafosAceleradoRTV (
                              double *probabilidad_fijacion, //Esto es el valor de retorno - asumo que hay memoria reservada
                              double *tiempoT,               //Tiempo medio en acabar (retorno)
                              double *err_tiempoT,           //El varianza/tamaño de lo anterior (retorno)
                              double *tiempoE,               //Tiempo medio en acabar con exito (retorno)
                              double *err_tiempoE,           //El varianza/tama de lo anterior (retorno)
                              double *tiempoF,               //Tiempo medio en acabar sin exito (retorno)
                              double *err_tiempoF,           //El varianza/tama de lo anterior (retorno)
                              const int *G_Vecinos,          //Matriz de vecinos N x N_1
                              const int *G_NVecinos,         //Vector de cantidad de vecinos N
                              const int N,                   //Orden del grafo
                              const int N_1,                 //Valencia máxima
                              const int G_Intentos,          //Numero de intentos para calcular la probabilidad de fijación
                              const double rMin,             //Valor minimo de r
                              const double rMax,             //Valor máximo de r (no se llega)
                              const int Num_r                //Número de pruebas entre rMin y rMax
                              ) {
    //typedef uint32_t FixP16_16;
    typedef uint64_t FixP32_32;
    
#define pow2_16 65536
#define pow2_32 4294967296ull
#define shift1_32 4294967296ull
    
    //TODO hacer coma fija "móvil" haciendo una 'educated guess'
    //TODO cambiar el paradigma de G_Vecinos... hacerla no cuadrada N*N_1 si no segun las necesidades...
    
    int j;

    fprintf(stderr, "[Moran OMP] LoopErasedRTV: Graphs of order %i ", N);

    //Inicializamos unas semillas para los generadores aleatorios
        #pragma omp parallel for private(j) shared(probabilidad_fijacion)
	for( j=0 ; j < Num_r ; j++ ) {
        
        double    r         = rMin + (rMax - rMin)/Num_r*j;
        FixP32_32 FixPr     = (FixP32_32)(r * pow2_32);
        int       Intentos  = G_Intentos;
        ulong     exitos    = 0;
        
        int           NMutantes;
        //int           k; //, kk, i, x, y, poskk, posx;
        FixP32_32 prob;
        FixP32_32 ProbFrontera;
        
        
        well1024      rng;
        /*
         .: Variables :.
         
         NMutantes    : Número de mutantes actualmente (entero)
         
         ProbFrontera : Volumen total (Probabilidad sin normalizar) de la frontera (FixP16_16)
         
         k, kk        : Vértices que se repoduce y muere respectivamente (vértice)
         
         poskk        : Posición del vértice kk dentro de la lista de vecinos de k (entero)
         
         rng          : Estado del generador de números
         
         
         .: Punteros :.
         
         
         NVecinosF   : vector de tamaño N cuyas entradas son
         NVecinosF[k] = número de vecinos del vértice k que
         son opuestos a él (entero)
         
         VecinosF    : 'matriz' de tamaño NxNVecinos(n) cuyas entradas son
         VecinosF[k][i] = |FALSE  si k y Vecinos[k][i] son iguales (mutantes o no)
         |TRUE   si k y Vecinos[k][i] son opuestos
         
         Mutante     : vector de tamaño N cuyas entradas son
         Mutante[k] = |TRUE   si k es mutante
         |FALSE  si k no es mutante
         
         Probabilidad: vector de tamaño N con entradas (FixP16_16)
         Probabilidad[k] = Probabilidad de elegir el vértice k
         para reproducirse sobre un vértice opuesto
         Salvo el reescalado por r*Nmutantes + (N-Nmutantes)
         */
        int*    NVecinosF       =       (int*)malloc(sizeof(int      ) * N);
        int*    Mutante         =       (int*)malloc(sizeof(int      ) * N);
        int**   VecinosF        =      (int**)malloc(sizeof(int*     ) * N);
        FixP32_32* Probabilidad = (FixP32_32*)malloc(sizeof(FixP32_32) * N);
        
        FixP32_32* InvNVecinos  = (FixP32_32*)malloc(sizeof(FixP32_32) * N);
        FixP32_32* InvRNVecinos = (FixP32_32*)malloc(sizeof(FixP32_32) * N);
        
        well1024_init_devrandom(&rng);
        //well1024_init(&rng, 20);
        //well1024_init_time(&rng);
        
        //Rellenamos los datos, ponemos la frontera a 0 y todo sin mutantes
        //Inicialización
        for( int x=0 ; x < N ; x++ ) {
            VecinosF[x]     = (int*)malloc(sizeof(int) * G_NVecinos[x]);
            InvNVecinos[x]  = (FixP32_32)( shift1_32 / ((FixP32_32)G_NVecinos[x]) );
            InvRNVecinos[x] = (FixP32_32)(     FixPr / ((FixP32_32)G_NVecinos[x]) );
            
            NVecinosF[x]    = 0;
            Mutante[x]      = FALSE;
            Probabilidad[x] = 0;
            
            for( int y=0 ; y < G_NVecinos[x] ; y++ )
                VecinosF[x][y] = FALSE;
        }
        
        double i_tiempoT     = 0;
        double i_var_tiempoT = 0;
        double i_tiempoE     = 0;
        double i_var_tiempoE = 0;
        double i_tiempoF     = 0;
        double i_var_tiempoF = 0;
        double delta;
        //Fin de la inicialización
        
        
        //TODO: Hace este bucle paralelo
        //Comenzamos el Montecarlo
		for( int i=0 ; i < Intentos ; i++ ) {
            
			//Ponemos el primer mutante
			int k = well1024_next_int(&rng, N);
			NMutantes  = 1;
			Mutante[k] = TRUE;
            
            /* Me preocupa el numérico de las probabilidades.
             Voy a optar por NO NORMALIZAR LAS PROBABILIDADES */
            
            //Las cantidades de la frontera
            NVecinosF[k]    = G_NVecinos[k]; //Todos los vecinos de k están en la frontera
            Probabilidad[k] = InvRNVecinos[k] * G_NVecinos[k];
            /*La probabilidad de elegir 'k' para reproducirse sera 'r'.
             Para evitar cosas no cero que debieran ser cero lo calculo así
             Falta dividir por NNormales + r (la normalización) */
            
            ProbFrontera    = Probabilidad[k]; /*El tamaño de toda la frontera.
                                                Se actualiza un poco más abajo, como antes,
                                                falta dividir por NNormales + r */
            
            for( int posx=0 ; posx < G_NVecinos[k] ; posx++) {
                int x = G_Vecinos[posx + N_1*k];
                                    //aqui G_Vecinos + N_1*x es donde empiezan los vecinos de x
                int posk = rebusca(G_Vecinos + N_1*x , G_NVecinos[x], k);
                NVecinosF[x]       = 1;     //que tienen un vecino en la frontera
                Probabilidad[x]    = InvNVecinos[x];
                ProbFrontera      += InvNVecinos[x];
                VecinosF[x][posk]  = VecinosF[k][posx] = TRUE;
            }
            
            int iteraciones = 0;
            //Empezamos hasta que lleguemos a la absorción
			while( (NMutantes < N) && (NMutantes > 0) ) {
                iteraciones++;
                
                //Un número aleatorio
                prob = (FixP32_32)( well1024_next_double(&rng) * ProbFrontera );
                /*Ahora $prob \in [0,ProbFrontera)\cap\mathbb{Z}$
                 Hay un problema muy grave si prob = 0 en el bucle siguiente,
                 podríamos hacer reproducirse algo que tuviese probabilidad 0 de hacerlo */
                prob++;
                /*Así $prob \in (0,ProbFrontera]\cap\mathbb{Z}$. Todo esto, y lo anterior hay que
                 entender ProbFrontera como el entero que es y no como el numero de punto fijo */
                
                
                //Tomamos el que se reproduce y el que se muere
                k=0;
                while (prob > Probabilidad[k]) {
                    prob -= Probabilidad[k++];
                }
                //k se va a reproducir
                
                int tmpkk = well1024_next_int(&rng, NVecinosF[k]);
                
                int poskk = busca(VecinosF[k], G_NVecinos[k], tmpkk);
                
                int kk    = G_Vecinos[poskk + N_1*k];
                //kk morirá
                
                //Actualizamos números de mutantes
                if( Mutante[k] ){
                    //Se ha reproducido un mutante
                    Mutante[kk] = TRUE;
                    NMutantes++;
                } else {
                    //Se ha reproducido un no mutante
                    Mutante[kk] = FALSE;
                    NMutantes--;
                }
                
                //Actualizamos las probabilidades y la frontera
                NVecinosF[kk] = G_NVecinos[kk] - NVecinosF[kk];
                ProbFrontera -= Probabilidad[kk];
                Probabilidad[kk] = (Mutante[kk]?InvRNVecinos[kk]:InvNVecinos[kk]) * NVecinosF[kk];
                
                ProbFrontera += Probabilidad[kk];
                
                for( int posx = 0 ; posx < G_NVecinos[kk] ; posx++) {
                    int x = G_Vecinos[posx + N_1*kk];
                    poskk = rebusca(G_Vecinos + N_1*x,G_NVecinos[x],kk);
                    if (VecinosF[kk][posx]) {
                        //Eran vecinos, y ahora perdemos un vecino en la frontera
                        NVecinosF[x]--;
                        Probabilidad[x]    -= (Mutante[x]?InvRNVecinos[x]:InvNVecinos[x]); //Pierde esta probabilidad
                        ProbFrontera       -= (Mutante[x]?InvRNVecinos[x]:InvNVecinos[x]); //También el total
                        VecinosF[kk][posx]  = VecinosF[x][poskk] = FALSE;
                    } else {
                        //No eran vecinos, y ahora ganamos un vecino en la frontera
                        NVecinosF[x]++;
                        Probabilidad[x]    += (Mutante[x]?InvRNVecinos[x]:InvNVecinos[x]); //Se añade esto esta probabilidad
                        ProbFrontera       += (Mutante[x]?InvRNVecinos[x]:InvNVecinos[x]); //También el total
                        VecinosF[kk][posx]  = VecinosF[x][poskk] = TRUE;
                    }
                }
			}

            
            //Guardamos el resultado y...
            //Estadisticas de tiempo generales
            //Usamos el algoritmo para la varianza de Knuth
            delta = (double)iteraciones - i_tiempoT;
            i_tiempoT = i_tiempoT + delta/(i + 1);
            i_var_tiempoT = i_var_tiempoT + delta*(iteraciones - i_tiempoT);
            
		if(NMutantes) {
                exitos++;
                delta = (double)iteraciones - i_tiempoE;
                i_tiempoE = i_tiempoE + delta/exitos;
                i_var_tiempoE = i_var_tiempoE + delta*(iteraciones - i_tiempoE);
                
                for(int x=0; x<N ; Mutante[x++] = FALSE);
                //Solo hace falta poner los mutantes a 0 si todos son mutantes
            } else {
                delta = (double)iteraciones - i_tiempoF;
                i_tiempoF = i_tiempoF + delta/(i - exitos + 1);
                i_var_tiempoF = i_var_tiempoF + delta*(iteraciones - i_tiempoF);
            }
        }
        //Final del Montecarlo
        
        //Limpiamos memoria
        free(Mutante);
        for( int x = 0 ; x < N ; x++ )
            free(VecinosF[x]);
        free(VecinosF);
        free(NVecinosF);
        free(Probabilidad);
        free(InvNVecinos);
        free(InvRNVecinos);
        
        probabilidad_fijacion[j] = ((double)exitos)/Intentos;
        
        tiempoT[j] = i_tiempoT;
        tiempoE[j] = i_tiempoE;
        tiempoF[j] = i_tiempoF;
        
        err_tiempoT[j] = i_var_tiempoT/((double)Intentos*Intentos);
        err_tiempoE[j] = (exitos)?(i_var_tiempoE/((double)exitos*exitos)):0;
        err_tiempoF[j] = (Intentos-exitos)?(i_var_tiempoF/(((double)(Intentos-exitos))*(Intentos-exitos))):0;
        
        fputc('.', stderr);
        /*fprintf(stderr, __MSG_CORTO__ , r, probabilidad_fijacion[j]);*/
	}
    
    fputc('\n', stderr);
                                  
    return 1;
}


