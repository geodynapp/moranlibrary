#include <stdlib.h>
#include <stdio.h>
#include "well1024.h"
#include "moran.h"



int EvolutivaGrafos (
                     double *probabilidad_fijacion, //Esto es el valor de retorno - asumo que hay memoria reservada
                     const int *G_Vecinos,          //Matriz de vecinos N x N_1
                     const int *G_NVecinos,         //Vector de cantidad de vecinos N
                     const int N,                   //Orden del grafo
                     const int N_1,                 //Valencia máxima
                     const int G_Intentos,          //Numero de intentos para calcular la probabilidad de fijación
                     const double rMin,             //Valor minimo de r
                     const double rMax,             //Valor máximo de r (no se llega)
                     const int Num_r                //Número de pruebas entre rMin y rMax
                     ) {
    
    int j;
    
    fprintf(stderr, "[Moran OMP] Montecarlo: Graphs of order %i ", N);
    
    //Inicializamos unas semillas para los generadores aleatorios
#pragma omp parallel for private(j) shared(probabilidad_fijacion) schedule(runtime)
	for( j=0 ; j < Num_r ; j++ ) {
        double r = rMin + (rMax - rMin)/Num_r*j;
        
        
        well1024 rng;
		well1024_init_devrandom(&rng);
        //well1024_init(&rng, 1);
        
        
		unsigned long exitos = 0;
        
        int *etiquetas = (int*)malloc(sizeof(int) * N);  // P_ETIQUETAS
        int *CMutantes = (int*)malloc(sizeof(int) * N);  // P_RANGO0;
        int *CNormales = (int*)malloc(sizeof(int) * N);  // P_RANGO1;
        
        int Intentos = G_Intentos;
        
		for( int i=0 ; i<Intentos ; i++ ) {
			//El grafo
            for( int x = 0 ; x < N ; x++){
                etiquetas[x] = 0;
                CNormales[x] = 1;
                CMutantes[x] = 0;
            }
            
			//Ponemos el primer mutante
			int kk;
			int k = well1024_next_int(&rng, N);
			int NMutantes = 1;
			etiquetas[k] = TRUE;
			CNormales[k] = FALSE;
			CMutantes[k] = TRUE;
            
			while( (NMutantes < N) && (NMutantes > 0) ) {
				/*Tomamos uno al azar para reproducirse
                 Usamos los conjuntos CNormales y CMutantes para buscar los elementos al azar.
                 P[CNormales] =  #CNormales/(#CNormales + r#CMutantes)
                 P[CMutantes] = r#CMutantes/(#CNormales + r#CMutantes)              */
				if( well1024_next_double(&rng) < ((double)(N-NMutantes))/(N-NMutantes + r*NMutantes) ){
					k = busca(CNormales, N, well1024_next_int(&rng, N-NMutantes));
				} else {
					k = busca(CMutantes, N, well1024_next_int(&rng, NMutantes));
				}
                
				//Tomamos otro al azar para morir
				kk = G_Vecinos[ well1024_next_int(&rng, G_NVecinos[k]) + N_1 * k]; //La matrix V[x][y] = G_V[y + x*N_1]
                
				if( etiquetas[k] && !etiquetas[kk] ) {
					NMutantes++;
					CMutantes[kk] = TRUE;
					CNormales[kk] = FALSE;
					etiquetas[kk] = etiquetas[k];
				} else if( !etiquetas[k] && etiquetas[kk] ) {
					NMutantes--;
					CMutantes[kk] = FALSE;
					CNormales[kk] = TRUE;
					etiquetas[kk] = etiquetas[k];
				}
				//En el resto de casos no hay nada que hacer
			}
            
			//Guardamos el resultado
			if(NMutantes)
				exitos++;
		}
        //Limpiamos memoria
        free(etiquetas);
        free(CMutantes);
        free(CNormales);
        
		probabilidad_fijacion[j] = ((double)exitos)/Intentos;
        
        fputc('.',stderr);
        
        //fprintf(stderr, __MSG_CORTO__ , r, probabilidad_fijacion[j]);
	}

    fputc('\n',stderr);
    return 1;
}


