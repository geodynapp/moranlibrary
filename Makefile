CC = gcc-5
ARCH=$(shell uname -m)
PLATFORM=$(shell uname -s)
CFLAGS=-c -fPIC -Wall -fopenmp -std=c99 -O2 -march=native -Wa,-q
LDFLAGS=-shared -lgomp 
SOURCES=MoranConTiempo.c MoranConTiempoAceleratoRTV.c MoranConTiempoR.c MoranSinTiempo.c MoranSinTiempoAcelerado.c busca.c utils.c well1024.c
OBJECTS=$(SOURCES:.c=.o)
LIB = libMoranOpenMP-$(PLATFORM)-$(ARCH).dylib 

all: $(SOURCES) $(LIB)
	
$(LIB): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.c.o:
	$(CC) $(CFLAGS) $< -o $@
	
clean:
	@rm *.o
