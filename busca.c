//
//  busca.c
//  MoranOpenMP
//
//  Created by Álvaro Lozano Rojo on 30/10/12.
//
//

#include "moran.h"

int busca(int *C, int N, int pos) {
    int j = -1;
    for( int i=0 ; i < N ; i++ )
        if(C[i])
            if(++j == pos)
                return i;
}

int rebusca(int *C, int N, int el) {
    for(int i=0 ; i < N; i++)
        if(C[i]==el)
            return i;
}

/*
int ubusca(const uchar *C, const int N, const int pos) {
    int j = -1;
    for( int i=0 ; i < N ; i++ )
        if(C[i])
            if(++j == pos)
                return i;
}*/
